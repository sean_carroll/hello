PROJECT_NAME ?= hello

clean:
	rm -rf bin/
	rm -rf cover/

mod:
	go mod tidy
	go mod vendor

build: clean mod
	go build -o bin/$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

test:
	go test ./... -count 1 -v -timeout 5s

